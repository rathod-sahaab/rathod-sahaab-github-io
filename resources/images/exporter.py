#!/usr/bin/python
import os
from sys import argv

options = {
        'path': '.',
        'dpi': '300'
        }


def react(x, y):
    global options
    options[x] = y


def argument_parser():
    i = 1
    global options
    while i < len(argv):
        if '-' in argv[i]:
            option = str(argv[i]).strip('-')
            if option in options.keys():
                react(option, argv[i+1])
                i += 1
        else:
            print('\nError parsing...\n')
        i += 1


def ext_to_png(x):
    return x[:-4]+'.png'


argument_parser()
print(options)
print(os.listdir(options['path']))
for f in os.listdir(options['path']):
    if f[-4:] == '.svg':

        query = 'inkscape -z -e {} -d {} {}/{}'.format(ext_to_png(f), options['dpi'], options['path'], f)
        os.system(query)
